const prompt = require("prompt-sync")();

const getCurrentFloor = (input) => {
  const openParenthesis = input.split("").filter((char) => char === "(").length;
  const closeParenthesis = input.length - openParenthesis;
  return openParenthesis - closeParenthesis;
};

const getFirstBasementMove = (input) => {
  let currentFloor = 0;
  for (let index = 0; index < input.length; index++) {
    const char = input[index];

    if (char === "(") {
      currentFloor += 1;
    } else {
      currentFloor -= 1;
    }

    if (currentFloor === -1) {
      return index + 1;
    }
  }
  return;
};

const main = () => {
  const input = prompt("Enter the input: ");
  const currentFloor = getCurrentFloor(input);
  console.log("1. Current floor :", currentFloor);

  const firstBasementMove = getFirstBasementMove(input);
  console.log("2. First basement move :", firstBasementMove);
};

main();
